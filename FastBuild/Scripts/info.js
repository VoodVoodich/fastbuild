﻿$('document').ready(function () {

    $('#i1').click(function () {
        $('#info').html('<h1>Bjorn</h1><p> Klasyczna kuchnia w odcieniu satynowej bieli dobrze współgra z dodatkami o dowolnej kolorystce. Lakierowane, frezowane fronty nadają szafkom wyjątkowego wyglądu. Wzbogacona w system cichego domyku.</p> <a target="_blank" href="http://localhost:52788/project/indoors/0">Learn more</a>');
        $('#display').html('<img src="../picture/1i.png">');
    });
    $('#i2').click(function () {
        $('#info').html('<h1>DARIA</h1><p>Zestaw mebli kuchennych DARIA to atrakcyjny zestaw mebli do kuchni wykonanych z płyty meblowej laminowanej, fronty szafek są w kolorze białym, natomiast korpusy w wybarwieniu dąb sonoma</p> <a target="_blank" href="http://localhost:52788/project/indoors/1">Learn more</a>');
        $('#display').html('<img src="../picture/2i.png">');
    });

    $('#i3').click(function () {
        $('#info').html('<h1>Matilda</h1><p>Klasyczna lakierowana kuchnia w jasnej szarości z nowoczesnym czarnym uchwytem. Lakierowane, frezowane fronty nadają szafkom wyjątkowego wyglądu. Wzbogacona w system cichego domyku. </p> <a target="_blank" href="http://localhost:52788/project/indoors/2">Learn more</a>');
        $('#display').html('<img src="../picture/3i.png">');
    });

    $('#i4').click(function () {
        $('#info').html('<h1>Kazuma</h1><p>Drewno, biel i blask w jednym. Mleczne, wykończone na wysoki połysk fronty, tworzą nowoczesną kompozycję. Drewniana struktura korpusu dodaje całości naturalnego charakteru. Meble wyposażone są w system cichego domyku oraz podnośniki pneumatyczne.</p>   <a target="_blank" href="http://localhost:52788/project/indoors/3">Learn more</a>');
        $('#display').html('<img src="../picture/4i.png">');
    });

    $('#i5').click(function () {
        $('#info').html('<h1>ASPEN</h1><p>Zestaw ASPEN to system mebli inspirowany skandynawskim wzornictwem, w którym połączenie białych frontów z kolorem dębu Lefkas doskonale podkreśla formę każdej z brył. Nowoczesny design wyrażający się w bezuchwytowych frontach.</p>   <a target="_blank" href="http://localhost:52788/project/indoors/4">Learn more</a>');
        $('#display').html('<img src="../picture/5i.png">');
    });

    $('#s1').click(function () {
        $('#info').html('<h1>Salon kąpielowy w odcieniach szarości | proj. formativ</h1><p>No to teraz mam wrażenie, że mój przegląd to przeplatanka tradycji z nowoczesnością. Wedle normy, przyszła kolej na ład, harmonię i prostotę, czyli wszystko to, co w klasyce najlepsze. Ponadto, jak sami widzicie jest to propozycja do nieco większych pomieszczeń… ale kto nas nie marzy o tym, aby mieć, i wannę (wolnostojącą), i prysznic? W tym projekcie to się zadziało. Żeby tego było mało, znajdziemy tu duże lustro i dwie umywalki nablatowe. </p>   <a target="_blank" href="http://localhost:52788/project/indoors/5">Learn more</a>');
        $('#display').html('<img src="../picture/6i.png">');
    });

    $('#s2').click(function () {
        $('#info').html('<h1>Szara łazienka + drewno i niespotykane meble z płyty OSB | proj. One Design Monika Pniewska </h1><p> Nasz przegląd otwiera bardzo industrialna przestrzeń. Definiuje ją przede wszystkim grafitowa cegła oraz całkowicie odjechany grzejnik w miedzianym wybarwieniu.</p>   <a target="_blank" href="http://localhost:52788/project/indoors/6">Learn more</a>');
        $('#display').html('<img src="../picture/7i.png">');
    });

    $('#s3').click(function () {
        $('#info').html('<h1>Łazienka w popielatych kolorach z akcentem marmuru i wzorzystą podłogą | proj. JT Grupa</h1><p>Przyznam szczerze, że ten projekt to jeden z moich faworytów. Mam wrażenie, że taka aranżacja nieco odbiega od tradycyjnego wyobrażenia o szarej łazience, ale to chyba dobrze? Po pierwsze, zamiast popularnego grafitu przypominającego beton, znalazły się tu subtelne popielate tony. To bardzo dobry wybór, biorąc pod uwagę mały metraż.</p>   <a target="_blank" href="http://localhost:52788/project/indoors/7">Learn more</a>');
        $('#display').html('<img src="../picture/8i.png">');
    });

    $('#s4').click(function () {
        $('#info').html('<h1>Mała łazienka w szarościach z akcentem niebieskiego i drewna | proj. Flow Interiors </h1><p>Poniżej projekt łazienki, który gdyby nie wzorzyste płytki na jednej ze ścian, niczym by się nie wyróżniała. Kafelki drewnopodobne i te imitujące beton architektoniczny to już połączenie kultowe. Jeśli jednak zależy Wam na dodaniu wnętrzu indywidualnego charakteru, warto postawić właśnie na patchworkowe płytki. Oczywiście nie można przesadzić. Jak widać, czasami wystarczy parę płytek, aby przeobrazić łazienkę w miejsce, w którym odbija się echo światowego designu.</p>   <a target="_blank" href="http://localhost:52788/project/indoors/8">Learn more</a>');
        $('#display').html('<img src="../picture/9i.png">');
    });

    $('#s5').click(function () {
        $('#info').html('<h1>Raj w beton ubrany | proj. MAKA.STUDIO </h1><p>Dacie wiarę, że ta łazienka jest częścią sypialni? Na taką fantazję mogło sobie pozwolić tylko biuro architektoniczne MAKA.STUDIO. Dobrze wiem, że o ile na wannę w pokojowym hotelu patrzycie pobłażliwym okiem, o tyle ta w domowej sypialni, już Wam totalnie nie pasuje. Nie na darmo jednak zastosowano tu zasłony, które z jednej strony zasłaniają ogromne okno, z drugiej zaś część sypialnianą. Ja już na sam widok tego projektu, mam ochotę zanużyć się w ogromnej wannie i spędzać w niej każdą wolną chwilę. </p>   <a target="_blank" href="http://localhost:52788/project/indoors/9">Learn more</a>');
        $('#display').html('<img src="../picture/10i.png">');
    });
});